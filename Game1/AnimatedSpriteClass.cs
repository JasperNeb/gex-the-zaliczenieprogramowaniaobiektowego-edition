﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

public class AnimatedSpriteClass
{
    public Texture2D Texture { get; set; }
    public int Rows { get; set; }
    public int Columns { get; set; }
    private int currentFrame;
    private int totalFrames;

    private int speed; //trzeba spowolnic niektore animacje xd


    public AnimatedSpriteClass(Texture2D texture, int rows, int columns)
    {
        this.Texture = texture;
        this.Rows = rows;
        this.Columns = columns;
        this.currentFrame = 0;
        this.totalFrames = Rows * Columns;
    }
    public void Update()
    {
        currentFrame = (currentFrame + 1) % totalFrames;
    }
    public void Draw(SpriteBatch spriteBatch, Vector2 location, bool flipped = false)
    {
        int width = Texture.Width / Columns;
        int height = Texture.Height / Rows;
        int row = (int)((float)currentFrame / (float)Columns);
        int column = currentFrame % Columns;

        Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
        Rectangle destinationRectangle = new Rectangle((int)location.X, (int)location.Y, width, height);
        SpriteEffects effect = new SpriteEffects();
        if (flipped)
        {
            effect = SpriteEffects.FlipHorizontally;
        }

        spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, Color.White, 0, new Vector2(0, 0), effect, 1);

    }
    public int CurrentFrame { get { return currentFrame;  } set { currentFrame = value;  } }
}
