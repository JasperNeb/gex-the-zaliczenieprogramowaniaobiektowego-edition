﻿

using Microsoft.Xna.Framework;

namespace Game1
{

    class Diamond
    {
        private AnimatedSpriteClass diamondSprite;
        private int x, y;
        public Rectangle d;
        public static int speedX;

        private bool visible = true;

        public Diamond(int x, int y)
        {
            this.x = x;
            this.y = y;
            d = new Rectangle(x, y, 15, 30);
            speedX = 0;
        }

        public void update()
        {
            speedX = Tile.speedX;
            d.Offset(-speedX, 0);
            System.Diagnostics.Debug.WriteLine("diamond update");
            if (d.Intersects(Game1.hero.bottom))
            {
                visible = false;
                Game1.scoreCurrent += 1;
                this.y -= 4000;
                d.Offset(0, -4000);
                System.Diagnostics.Debug.WriteLine("zebrany");
            }
        }

        public AnimatedSpriteClass DiamondSprite
        { get { return diamondSprite; } set { diamondSprite = value; } }
        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }
        public bool Visible { get { return visible; } set { visible = value; } }

    }
}
