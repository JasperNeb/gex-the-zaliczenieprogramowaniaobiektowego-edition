﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using System.IO;
using System;
using System.Linq;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    /// 

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Texture2D background, tileDirt, tileGrass;
        public static Hero hero;
        ArrayList tiles = new ArrayList();
        ArrayList diamonds = new ArrayList();
        AnimatedSpriteClass diamondSpriteAnimated, mushroomSprite, portalSprite;
        ArrayList mushrooms = new ArrayList();
        Portal portal = new Portal(100, 0); //TODO napraw
        int currentLevel;
        int levelCount = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "maps")).Length - 1; //-1, for error.txt
        int length;
        //score
        int scoreSaved = 0; //permament one
        public static int scoreCurrent = 0; //for current level only, reset on level reset
        //text with points
        SpriteFont font;
        string scoreText = "Wynik: {0}";


        //public static Background background1, background2;

        //things used to slow down the game
        double millisecondsPerFrame = 20;
        double timeSinceLastUpdate = 0; //Accumulate the elapsed time
        double animationDelay = 100; //independent counter to slow down diamonds and mushrooms animations
        double animationCounter = 0;

        private void generateTiles(int levelNumber)
        {
            if (levelNumber > levelCount)
            {
                //end of the game
                scoreText = "Wynik ostateczny: {0}";
            }
            else
            {
                string[] fileContent;
                try
                {
                    fileContent = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "maps", levelNumber.ToString() + ".txt")); //<exe>\maps\<filename>.txt
                }
                catch
                {
                    fileContent = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "maps", "error.txt")); //file not found
                }
                Array.Resize(ref fileContent, Constants.TILES_VERTICALLY);//only TILES_VERTICALLY first lines are useful, other are comments

                length = fileContent.OrderByDescending(s => s.Length).First().Length;
                System.Diagnostics.Debug.WriteLine("Długość mapy: {0}", length);

                currentLevel = levelNumber;
                //clear old data
                tiles.Clear();
                mushrooms.Clear();
                diamonds.Clear();


                for (int i = 0; i < Constants.TILES_VERTICALLY; i++)
                {
                    for (int j = 0; j < length; j++)
                    {
                        System.Diagnostics.Debug.WriteLine("płytka {0},{1}", i, j);
                        char gridElement = (fileContent[i].Length < length ? '.' : fileContent[i][j]);
                        switch (gridElement)
                        {
                            case 'g': //grass
                                Tile t = new Tile(j, i, 0); //TODO chceck for dirt/grass
                                tiles.Add(t);
                                break;
                            case 'm': // mushroom;
                                Mushroom m = new Mushroom(j * 40, i * 40);
                                mushrooms.Add(m);
                                break;
                            case 'd': //diamond
                                Diamond d = new Diamond(j * 40, i * 40);
                                diamonds.Add(d);
                                break;
                            case ' ':
                            case '.':
                            default: //every unknown symbol is defaulted to air
                                break;
                        }
                    }
                }
                System.Diagnostics.Debug.WriteLine("Portal na pozycji {0},", length * 40);
                portal.X = length * 40;

                //put hero in place
                //TODO zrób ładniej
                hero.SpeedX = 1;
                hero.SpeedY = 1;
                hero.CenterX = 45;
                hero.CenterY = 1;
                hero.bottom.X = 1;
                hero.bottom.Y = 1;
                hero.Alive = true;
            }
        }

        public Game1(int levelNumber = 1)
        {

            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = Constants.WINDOW_HEIGHT;
            graphics.PreferredBackBufferWidth = Constants.WINDOW_WIDTH;
            Content.RootDirectory = "Content";

            hero = new Hero();
            generateTiles(levelNumber);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>("stars");
            Texture2D texture = Content.Load<Texture2D>("gexherosheetrunning");
            hero.Running = new AnimatedSpriteClass(texture, 2, 8);
            texture = Content.Load<Texture2D>("gexstandingsheet");
            // texture = Content.Load<Texture2D>("standlol");
            hero.Standing = new AnimatedSpriteClass(texture, 1, 1);
            texture = Content.Load<Texture2D>("jumpingsheet");
            hero.Jumping = new AnimatedSpriteClass(texture, 1, 3);
            texture = Content.Load<Texture2D>("attackingsheet");
            hero.Attacking = new AnimatedSpriteClass(texture, 1, 8);
            texture = Content.Load<Texture2D>("diamondpurple");
            diamondSpriteAnimated = new AnimatedSpriteClass(texture, 1, 6);
            texture = Content.Load<Texture2D>("mushroomspritesheet");
            mushroomSprite = new AnimatedSpriteClass(texture, 1, 3);
            texture = Content.Load<Texture2D>("portal");
            portalSprite = new AnimatedSpriteClass(texture, 1, 1);

            font = Content.Load<SpriteFont>("font");

            tileDirt = Content.Load<Texture2D>("dirt");
            tileGrass = Content.Load<Texture2D>("grass");

            hero.Current = hero.Standing;



            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            animationCounter += gameTime.ElapsedGameTime.TotalMilliseconds;
            timeSinceLastUpdate += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (timeSinceLastUpdate >= millisecondsPerFrame)
            {
                timeSinceLastUpdate = 0;

                KeyboardState state = Keyboard.GetState();
                if (state.IsKeyDown(Keys.Escape))
                {
                    Exit();
                }
                if (currentLevel <= levelCount)
                {
                    if (state.IsKeyDown(Keys.Right))
                    {
                        hero.Current = hero.Running;
                        hero.moveRight();
                    }

                    else if (state.IsKeyDown(Keys.Left))
                    {
                        hero.Current = hero.Running;
                        hero.moveLeft();
                    }
                    else
                    {
                        hero.stop();
                        hero.Current = hero.Standing;

                    }

                    if (state.IsKeyDown(Keys.A) || state.IsKeyDown(Keys.Up)) //TODO jump
                    {
                        System.Diagnostics.Debug.WriteLine("hophop");

                        hero.jump();
                    }

                    if (state.IsKeyDown(Keys.Space))
                    {
                        System.Diagnostics.Debug.WriteLine("atak");
                        hero.attack();
                    }
                    if (state.IsKeyDown(Keys.R))
                    {
                        scoreCurrent = 0;
                        generateTiles(currentLevel);
                    }
                }
                else
                {
                    hero.stop();
                    hero.Current = hero.Standing;
                }

                foreach (Tile t in tiles)
                {
                    t.update();
                }
                foreach (Diamond d in diamonds)
                {
                    d.update();

                }
                foreach (Mushroom m in mushrooms)
                {
                    m.update();
                }

                hero.update();


                if (portal.update()) //jump to te next level???
                {
                    scoreSaved += scoreCurrent;//now we can permamently add points for this level
                    scoreCurrent = 0;
                    generateTiles(++currentLevel);
                }

                if (!hero.Alive)
                {
                    scoreCurrent = 0;
                    generateTiles(currentLevel);
                }

                if (animationCounter >= animationDelay)
                {
                    animationCounter = 0;
                    diamondSpriteAnimated.Update();
                    mushroomSprite.Update();
                    portalSprite.Update();
                }

                base.Update(gameTime);
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();
            spriteBatch.Draw(background, new Rectangle(0, 0, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT), Color.White);
            foreach (Tile t in tiles)
            {
                spriteBatch.Draw(tileGrass, new Vector2(t.TileX, t.TileY), Color.White);
            }
            foreach (Diamond d in diamonds)
            {
                diamondSpriteAnimated.Draw(spriteBatch, new Vector2(d.d.X, d.d.Y));
            }
            foreach (Mushroom m in mushrooms)
            {
                mushroomSprite.Draw(spriteBatch, new Vector2(m.R.Location.X, m.R.Location.Y));
            }
            hero.Current.Draw(spriteBatch, new Vector2(hero.bottom.Location.X, hero.bottom.Location.Y), !hero.IsMovingRight);
            portalSprite.Draw(spriteBatch, new Vector2(portal.R.Location.X, portal.R.Location.Y));

            spriteBatch.DrawString(font, String.Format(scoreText, scoreCurrent + scoreSaved), new Vector2(0, 0), Color.DarkViolet);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
