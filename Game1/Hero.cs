﻿

using Microsoft.Xna.Framework;

namespace Game1
{
    public class Hero
    {
        private int centerX = 45;
        private int centerY = 0;
        private bool jumped = false;
        private bool isattacking = false;
        private bool isMovingRight = false;
        private int hp = 4;
        private bool alive = true;
        private double attackingtime = 0;

        private int speedX = 0;
        private int speedY = 1;
        private AnimatedSpriteClass standing;
        private AnimatedSpriteClass running;
        private AnimatedSpriteClass jumping;
        private AnimatedSpriteClass attacking;
        private AnimatedSpriteClass current;

        public Rectangle head, left, right, bottom;

        public Hero()
        {
            // System.Diagnostics.Debug.WriteLine("konstruktor domyslnu");
            //this.boundingbox = new Rectangle(centerX - 20, centerY - 10, 40, 80);
            this.bottom = new Rectangle(centerX, centerY, 16, 28); //bo tak.
                                                                   // this.body = new Rectangle(centerX + 3, centerY -10, 10, 20);
        }

        //TODO zamiast centerx i centery mozna uzywac wylacznie bottom.Location
        public Hero(int x, int y)
        {
            this.centerX = x;
            this.centerY = y;


        }

        public void update()
        {
            if (bottom.Location.Y > 800)
            {
                alive = false; //GEJM OWER
            }

            if (isattacking)
            {
                if (attackingtime > 8)
                {
                    isattacking = false;

                    current = standing;
                    attackingtime = 0;

                }
                else
                {
                    attackingtime++;
                }

            }

            // Updates X position
            if (speedX < 0)
            {
                //centerX += speedX;
                bottom.Offset(speedX, 0);

            }

            // speed is 0 
            if (speedX == 0)
            {
                //don't scroll floor
                Tile.speedX = 0;


            }

            //we are far than 150px to thr right edge, speed is positive
            if (centerX <= Constants.WINDOW_WIDTH - 150 && speedX > 0)
            {
                // centerX += speedX;
                bottom.Offset(speedX, 0);
                Tile.speedX = 0; //TODO - to moze byc 1 zmienna w sumie.

            }

            //we are closer than 150px to thr right edge, speed is positive
            if (speedX > 0 && bottom.Location.X > Constants.WINDOW_WIDTH - 150)
            {
                //speedX to 0, scrollowanie podlogi
                speedX = 0;
                bottom.Offset(-Constants.MOVESPEED, 0);
                Tile.speedX = Constants.MOVESPEED;

            }


            // Updates Y Position
            // centerY += speedY;
            bottom.Offset(0, speedY);

            // Handles Jumping

            speedY += 1;

            if (speedY > 3)
            {
                current = jumping;
            }
            System.Diagnostics.Debug.WriteLine(jumped);


            // Prevents going beyond X coordinate of 0
            if (bottom.Left < 70 && speedX < 0)
            {
                //na chuj centerx i Y
                //nie przeklinaj w kodzie :C
                //centerX = 0;
                //centerY = 0;
                speedX = 0;
                bottom.Offset(Constants.MOVESPEED, 0);
                Tile.speedX = -Constants.MOVESPEED;

            }
            current.Update();
        }

        public void moveRight()
        {
            speedX = Constants.MOVESPEED;
            isMovingRight = true;
        }

        public void moveLeft()
        {
            speedX = -Constants.MOVESPEED;
            isMovingRight = false;
        }

        public void stop()
        {
            speedX = 0;
        }

        public void jump()
        {
            if (jumped == false)
            {
                bottom.Offset(0, -1);
                System.Diagnostics.Debug.WriteLine("hophop");
                speedY = -Constants.JUMPSPEED;
                jumped = true;
                current = jumping;
            }

        }
        public void attack()
        {
            this.isattacking = true;
            current = attacking;
        }
        public int CenterX
        {
            get { return centerX; }
            set { centerX = value; }
        }
        public int CenterY
        {
            get { return centerY; }
            set { centerY = value; }
        }

        public int SpeedX
        {
            get { return speedX; }
            set { speedX = value; }
        }
        public int SpeedY
        {
            get { return speedY; }
            set { speedY = value; }
        }
        public AnimatedSpriteClass Running
        {
            get { return running; }
            set { running = value; }
        }

        public bool Jumped
        {
            set { jumped = value; }
        }
        public bool Alive
        {
            get { return alive; }
            set { alive = value; }
        }
        public AnimatedSpriteClass Standing
        {
            get { return standing; }
            set { standing = value; }
        }
        public AnimatedSpriteClass Current
        {
            get { return current; }
            set { current = value; }
        }
        public AnimatedSpriteClass Jumping
        {
            get { return jumping; }
            set { jumping = value; }
        }
        public AnimatedSpriteClass Attacking
        {
            get { return attacking; }
            set { attacking = value; }
        }
        public bool IsMovingRight
        {
            get { return isMovingRight; }
            set { isMovingRight = value; }
        }

    }
}
