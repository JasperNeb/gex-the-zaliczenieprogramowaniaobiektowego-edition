﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game1
{

    class Portal
    {
        private int x, y;
        private Rectangle r;
        public static int speedX;

        public Portal(int x, int y)
        {
            this.x = x;
            this.y = y;
            speedX = 0;
            r = new Rectangle(x, y, 40, 480);
        }

        public bool update()
        {
            speedX = Tile.speedX;
            r.Offset(-speedX, 0);

            if (r.Intersects(Game1.hero.bottom))
            {
                Rectangle overlap = Rectangle.Intersect(Game1.hero.bottom, r);
                this.x = -4000;
                r.Offset(-4000, 0);
                return true; //should update
                // TODO obsługa przejścia
            }
            else
            {
                return false;
            }
        }

        public Rectangle R { get { return r; } }
        public int X
        {
            get { return x; }
            set
            {
                this.x = value;
                r.X = value;
            }
        }

    }
}
