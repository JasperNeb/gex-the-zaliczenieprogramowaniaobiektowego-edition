﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Game1
{
    class Tile
    {
        private Texture2D imggrass;
        private int tileX, tileY, type;
        private Rectangle r;
        public static int speedX = 0;


        public Tile(int x, int y, int type)
        {
            tileX = x * 40;
            tileY = y * 40;

            this.type = type;

            r = new Rectangle(tileX, tileY, 40, 40);

        }

        public void update()
        {

            //speedX = bg.getSpeedX() * 5;
            tileX -= speedX;
            r.Offset(-speedX, 0);
            // System.Diagnostics.Debug.WriteLine("TILE UPDATE");
            // System.Diagnostics.Debug.WriteLine(Game1.hero.bottom.Center);
            //  System.Diagnostics.Debug.WriteLine(r.Center);
            if (r.Intersects(Game1.hero.bottom))
            {
                Rectangle overlap = Rectangle.Intersect(Game1.hero.bottom, r);
                Game1.hero.SpeedY = 0;
                Game1.hero.bottom.Offset(0, -(overlap.Size.Y-1)); // FIX ME PLZ jak sie zmniejszy przesuniecie o 1 to przestaje sie trzasc, ale jednoczesnie przestaje skakac. kurwa.
                //Game1.hero.body.Offset(0, (Game1.hero.bottom.Bottom - (Game1.hero.Current.Texture.Height / Game1.hero.Current.Rows)) - 1);
                System.Diagnostics.Debug.WriteLine("kolizja");
                Game1.hero.Jumped = false;


            }

        }



        public Texture2D Imggrass { get { return imggrass; } set { imggrass = value; } }
        public int TileX { get { return tileX; } set { tileX = value; } }
        public int TileY { get { return tileY; } set { tileY = value; } }
        public int Type { get { return type; } set { type = value; } }
    }
}
